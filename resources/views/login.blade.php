<head>
    <title>Login</title>
</head>
<!------ Include the above in your HEAD tag ---------->
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

<body>
    <div id="login">
        <h3>Login</h3>
        <form id="login-form" action="{{ route('login') }}" method="post">
            @csrf
            <h3>Login</h3>
            <div>
                <label for="name">Username:</label><br>
                <input id="name" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            </div>
            <div>
                <label for="password">Password:</label><br>
                <input id="password" type="password" name="password" required autocomplete="current-password" >
            </div>
            <div>
                <input type="submit" name="submit" value="Submit">
            </div>
        </form>
    </div>
</body>