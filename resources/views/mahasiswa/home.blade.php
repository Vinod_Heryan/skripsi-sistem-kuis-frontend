@extends('layouts.mahasiswa')
@section('content')
@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
@if($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
<center>
<img id="ukrim" src="{{asset('images/ukrim.png')}}" width=100 height=100>
<div style="font-family: 'Poppins" >
<div id="posisicard" class="card" style="width: 70%;">
    <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{{route('kuis.mhs.tes')}}" method="get">
                        @csrf
                            <h1 id="tulisan" class="text-center text-dark">Masuk Kuis</h1>
                            <div class="form-group">
                                <input id="name" type="text" name="kode" placeholder="Masukan Kode Kuis" required class="form-control">
                            </div>
                            <div class="form-group">
                                <input id="posisimasuk" type="submit" name="submit" class="btn btn-dark btn-md" value="Masuk">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</center>
<style>
#posisicard {
    margin-top: 2%;
}
#ukrim {
    margin-top: 2%;
}
#tulisan {
    margin-top: 10%;
    margin-bottom: 20%;
}
#posisimasuk {
  margin-top: 15%;
  padding-left: 25px;
  padding-right: 25px;
  margin-bottom: 5%;
}
}
</style>
@endsection