@extends('layouts.mahasiswa')
@section('content')
    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<nav class="navbar navbar-light bg-light">
</nav>
<?php
    $no=1;
?>
<div style="font-family: 'Poppins" >
    <div class="d-flex justify-content-center">
	   <div class="card" style="width: 80%; margin-top: 3%;">
       <div class="card-body">
		<div class="d-flex justify-content-center">
			<p class="blockquote" style="width: 80%; margin-top: 3%;">
                @if($data['data']['data']['0']['status'] == 0)

                KUIS SUDAH SELESAI

                @else

                {{$data['data']['data']['0']['soal']}}

                @endif
                
            </p>
		</div>
        @if($halaman == $halaman_terakhir || $data['data']['data']['0']['status'] == 0)
		<form action="{{route('selesai')}}" method="post">
        @else
        <form action="{{route('jawab')}}" method="post">
        @endif
        {{csrf_field()}}
        <div class="row">

        @if($data['data']['data']['0']['status'] != 0)

        <input type="hidden" name="nomor" class="form-control" value="{{$halaman}}" required="required">
        @foreach($data['pilihan'] as $d)
            <span class="card text-white bg-secondary mb-3" style="width: 20%; margin: 2% 15% 2% 15%;">
    		    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="jawaban" id="inlineRadio1" value="{{$d['id_jawaban']}}" required="required">
                    <label class="form-check-label" for="inlineRadio1"><h5>{{$d['jawaban']}}</h5></label>
                </div> 
			</span>
        @endforeach

        @endif

		</div>
        </br>
        <div class="d-flex flex-row-reverse">
        @if($halaman == $halaman_terakhir || $data['data']['data']['0']['status'] == 0)
		<input type="submit" class="btn btn-success" value="Selesai">
        @else
        <input type="submit" class="btn btn-success" value="Jawab">
        @endif
        </div>
        </form> 
		</div>
       </div>
    </div>
</div>
@endsection