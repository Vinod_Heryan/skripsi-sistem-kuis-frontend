@extends('layouts.dosen')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="d-flex justify-content-center">
<div class="card" style="width: 95%; margin-top: 2%;">
<div class="card-body">
<nav class="navbar d-flex justify-content-end">

  <form class="form-inline" action="{{route('grup.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Nama Grup Sesi" name="search" aria-label="Search">
    <button class="btn btn-outline-info" type="submit">Search</button>
  </form>
 
</nav>
<div class="table-responsive">
<table class="table  table-striped table-bordered" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="75%">Nama Grup Sesi</th>
        <th class="text-center" width="20%">Actions</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($grup as $g)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td>{{$g['grup']}}</td>
        <td class="text-center">
        <button type="button" class="btn btn-outline-success my-2 my-sm-0" data-toggle="modal" data-target="#myModalRekap{{str_replace(' ', '', $g['grup'])}}"  title="Rekap Semua Nilai"><i class="fas fa-file-export"></i></button>
        <button type="button" class="btn btn-outline-warning my-2 my-sm-0" data-toggle="modal" data-target="#myModalUpdate{{str_replace(' ', '', $g['grup'])}}"  title="Edit Grup"><i class="fas fa-edit"></i></button>
        <a href="/dosen/grup/detail/{{$g['grup']}}" class="btn btn-outline-info my-2 my-sm-0"  title="Detail Grup"><i class="far fa-file-alt"></i></a>
      </td>
    </tr>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalRekap{{str_replace(' ', '', $g['grup']);}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Rekap Semua Nilai</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="d-flex justify-content-center">
            <a href="/dosen/nilai/rekap/semua/{{$g['grup']}}" class="btn btn-outline-danger my-2 my-sm-0"  title="Format PDF"><i class="far fa-file-pdf"></i></a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/dosen/nilai/rekap/semua/excel/{{$g['grup']}}" class="btn btn-outline-success my-2 my-sm-0"  title="Format Excel"><i class="far fa-file-excel"></i></a>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div> 
    </div>
  </div>
</div>

    <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdate{{str_replace(' ', '', $g['grup']);}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Grup</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('grup.update')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="grup" class="form-control" value="{{$g['grup']}}" required="required">

        <div class="form-group">
        <label for="namagrup">Nama Grup :</label>
        <input type="text" name="grupbaru" class="form-control" value="{{$g['grup']}}" id="namagrup" placeholder="Masukan Nama Kuis" required="required">
        </div>
        
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

    @endforeach
</table>
</div>
</div>
</div>
</div>
@endsection