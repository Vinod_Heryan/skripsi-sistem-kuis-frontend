<?php

namespace App\Http\Controllers\DosenController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class dosenKuisController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('dosen');
    }

    public function halamanKuis()
    {
        $id = session('id_dosen');
        // session(['page' => 1]);
        $url ="http://localhost:8000/dosen/kuis/$id";
        $token = session('token');
        $data = Http::withToken($token)->get($url)['data'];
        // $kuis = $data['data'];
        // $nama = $kuis['data'];
        // $halaman = $kuis['current_page'];
        // $jumlahPage = $kuis['last_page'];
        if($data == null)
        {
            return view('dosen/kosong/datakuis');

            // return $id;
        }

        return view('dosen/datakuis',compact('data'));
    }

    public function searchKuis(Request $request)
    {
        $id = session('id_dosen');
        // session(['page' => 1]);
        $url ="http://localhost:8000/dosen/kuis/$id/$request->search";
        $token = session('token');
        $data = Http::withToken($token)->get($url)['data'];
        // $kuis = $data['data'];
        // $nama = $kuis['data'];
        // $halaman = $kuis['current_page'];
        // $jumlahPage = $kuis['last_page'];

        return view('dosen/datakuis',compact('data'));
    }

    public function prosesUpdateKuis(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/kuis/update', [
            'id' => $request->id,
            'name' => $request->name, 
        ]);

        return redirect()->route('kuis')->with('success','Data Berhasil Diupdate !!!');
    }

    public function prosesAddKuis(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->post('http://localhost:8000/dosen/kuis/add', [
            'name' => $request->name, 
        ]);

        return redirect()->route('kuis')->with('success','Data Berhasil Ditambah !!!');
    }

    public function detailKuis($id,Request $request)
    {
        $url ="http://localhost:8000/dosen/kuis/soal/detail/$id";
        $token = session('token');
        $data = Http::withToken($token)->get($url)['data'];
        $id = Http::withToken($token)->get($url);

        if($data == null)
        {
            return view('dosen/kosong/detailkuis',compact('id'));

            // return $id;
        }
        
        return view('dosen/detailkuis',compact('data','id'));

        // return dd($data);

        // return $gambar;
    }

    public function prosesAddSoal(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->post('http://localhost:8000/dosen/kuis/soal/add', [
            'kuis_id'=>$request->kuis_id,
            'soal'=>$request->soal,
        ]);

        return back()->with('success','Data Berhasil Ditambahkan !!!');
    }

    public function prosesUpdateSoal(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/kuis/soal/update', [
            'id'=>$request->id,
            'soal'=>$request->soal,
        ]);

        return back()->with('success','Data Berhasil Diupdate !!!');
    }

    public function prosesAddPilihan(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->post('http://localhost:8000/dosen/kuis/soal/pilihan/add', [
            'soal_id'=>$request->soal_id,
            'jawaban'=>$request->jawaban,
            'kondisi'=>$request->kondisi,
        ]);

        if($request->kondisi == "")
        {
            return back()->with('error','Tambah Data Gagal Option Tidak Boleh Kosong !!!');
        }

        return back()->with('success','Data Berhasil DiTambahkan !!!');

        // $data = [ 'soal_id'=>$request->soal_id,
        //     'jawaban'=>$request->jawaban,
        //     'kondisi'=>$request->kondisi,];

        // return dd($data);
    }

    public function prosesUpdatePilihan(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/kuis/soal/pilihan/update', [
            'id'=>$request->id,
            'jawaban'=>$request->jawaban,
            'kondisi'=>$request->kondisi,
        ]);

        return back()->with('success','Data Berhasil Diupdate !!!');

        // $data = ['id'=>$request->id,
        // 'jawaban'=>$request->jawaban,
        // 'kondisi'=>$request->kondisi,];

        // return dd($data);
    }

    public function prosesDeleteSoal($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/kuis/soal/delete/$id";
        $data = Http::withToken($token)->delete($url);

        // if($data['status'] == '0'){

        //     return back()->with('error',$data['message'].' !!!');

        // }

        return back()->with('success','Data Berhasil DiHapus !!!');
    }

    public function prosesDeletePilihan($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/kuis/soal/pilihan/delete/$id";
        $data = Http::withToken($token)->delete($url);


        return back()->with('success','Data Berhasil DiHapus !!!');
    }

    public function prosesDeleteKuis($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/kuis/delete/$id";
        $data = Http::withToken($token)->delete($url);

        if($data['status'] == '0'){

            return back()->with('error',$data['message'].' !!!');

        }

        return back()->with('success','Data Berhasil DiHapus !!!');
    }

    // public function pageNextKuis()
    // {
    //     $id = session('id_dosen');
    //     session(['page' => session('page') + 1]);
    //     $page = session('page');
    //     $url ="http://localhost:8000/dosen/kuis/$id?page=$page";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url);
    //     $kuis = $data['data'];
    //     $nama = $kuis['data'];
    //     $halaman = $kuis['current_page'];
    //     $jumlahPage = $kuis['last_page'];

    //     return view('dosen/datakuis',compact('nama','halaman','jumlahPage'));
    // }

    // public function pageBackKuis()
    // {
    //     $id = session('id_dosen');
    //     session(['page' => session('page') - 1]);
    //     $page = session('page');
    //     $url ="http://localhost:8000/dosen/kuis/$id?page=$page";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url);
    //     $kuis = $data['data'];
    //     $nama = $kuis['data'];
    //     $halaman = $kuis['current_page'];
    //     $jumlahPage = $kuis['last_page'];

    //     return view('dosen/datakuis',compact('nama','halaman','jumlahPage'));
    // }

    // public function halamanUpdateKuis($id)
    // {
    //     $url ="http://localhost:8000/dosen/kuis/update/$id";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url)['data'];

    //     $id = $data['id'];
    //     $nama = $data['name'];

    //     return view('dosen/formupdatekuis',compact('id','nama'));

    //     // return dd($data);
    // }

    // public function halamanAddKuis()
    // {
    //     return view('dosen/formaddkuis');
    // }

    // public function halamanAddSoal($id)
    // {
    //     $url ="http://localhost:8000/dosen/kuis/soal/byidkuis/$id";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url)['data'];

    //     return view('dosen/formaddsoal',compact('data'));
    // }

    // public function halamanUpdateSoal($id)
    // {
    //     $url ="http://localhost:8000/dosen/kuis/soal/byid/$id";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url)['data'];

    //     return view('dosen/formupdatesoal',compact('data'));

    //     // return dd($data);
    // }

    // public function halamanAddPilihan($id)
    // {
    //     $url ="http://localhost:8000/dosen/kuis/soal/pilihan/byidSoal/$id";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url)['data'];

    //     return view('dosen/formaddpilihan',compact('data'));

    //     // return dd($data);
    // }

    // public function halamanUpdatePilihan($id)
    // {
    //     $url ="http://localhost:8000/dosen/kuis/soal/pilihan/byid/$id";
    //     $token = session('token');
    //     $data = Http::withToken($token)->get($url)['data'];

    //     return view('dosen/formupdatepilihan',compact('data'));

    //     // return dd($data);
    // }
    
}
