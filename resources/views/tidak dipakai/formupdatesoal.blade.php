@extends('layouts.dosen')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Tambah Soal</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('soal.update')}}" method="post">
{{csrf_field()}}
    <div class="form-group">
        <input type="hidden" name="id" class="form-control" value="{{$data['id']}}" required="required">
        <label for="soalkuis">Soal</label>
        <textarea name="soal" class="form-control" placeholder="Masukan Soal Disini" id="soalkuis"required="required">{{$data['soal']}}</textarea>
    </div>
        <input type="submit" class="btn btn-primary" value="Submit">
</form> 
</table>
</div>
@endsection