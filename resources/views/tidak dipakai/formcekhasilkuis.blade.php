@extends('layouts.mahasiswa')
@section('content')
@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="card-header">
<h2>Lihat Hasil Kuis Mahasiswa</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('nilai.mhs')}}" id="usrform" method="get">
{{csrf_field()}}
    <div class="form-group">
        <label for="kodesesi" >Kode Sesi</label>
        <input type="text" name="kode" class="form-control" id="kodesesi" placeholder="Masukan Kode Sesi" required="required">
    </div>
        <input type="submit" class="btn btn-primary" value="Submit">
</form> 
</table>
</div>
<!-- <style>
#posisi {
  padding: 125px;
}
#tulisan {
  margin-bottom: 30px;
}
#posisimasuk {
  margin-top: 30px;
  margin-left: 40%;
  padding-left: 25px;
  padding-right: 25px;
}
}
</style> -->
@endsection