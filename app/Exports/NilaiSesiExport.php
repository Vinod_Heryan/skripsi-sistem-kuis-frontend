<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Http;

class NilaiSesiExport implements FromView
{
    
    public function view(): View
    {
        $id = session('rekap_id');
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/$id";
        $data = Http::withToken($token)->get($url)['data'];

        return view('dosen/rekapnilaiexcel', [
            'data' => $data,
        ]);
    }
}
