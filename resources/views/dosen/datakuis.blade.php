@extends('layouts.dosen')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="d-flex justify-content-center">
<div class="card" style="width: 95%; margin-top: 2%;">
<div class="card-body">
<nav class="navbar">
<h3><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#myModalAdd" title="Tambah Kuis"><i class="fa fa-plus-square"></i></button></h3>
  <form class="form-inline" action="{{route('kuis.search')}}" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search Nama Kuis" name="search" aria-label="Search">
    <button class="btn btn-outline-info" type="submit">Search</button>
  </form>
</nav>
<div class="table-responsive">
<table class="table table-striped table-bordered" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="75%">Nama Kuis</th>
        <th class="text-center" width="20%">Actions</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $n)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td>{{$n['name']}}</td>
        <td class="text-center">
        <button type="button" class="btn btn-outline-warning my-2 my-sm-0" data-toggle="modal" data-target="#myModalUpdate{{$n['id']}}" title="Edit Kuis"><i class="fas fa-edit"></i></button>
        <a href="/dosen/kuis/soal/detail/{{$n['id']}}" class="btn btn-outline-info my-2 my-sm-0" title="Detail Kuis"><i class="far fa-file-alt"></i></a>
        <button type="button" class="btn btn-outline-danger my-2 my-sm-0" data-toggle="modal" data-target="#myModalDelete{{$n['id']}}" title="Hapus Kuis"><i class="fas fa-trash"></i></button>
      </td>
    </tr>

  <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdate{{$n['id']}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Kuis</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('kuis.update')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="id" class="form-control" value="{{$n['id']}}" required="required">

        <div class="form-group">
        <label for="namakuis">Nama Kuis</label>
        <input type="text" name="name" class="form-control" value="{{$n['name']}}" id="namakuis" placeholder="Masukan Nama Kuis" required="required">
        </div>
        
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalDelete{{$n['id']}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Kuis</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="text-center">Semua Data Dari Kuis " {{$n['name']}} " Akan Dihapus.</div>
        </div>
        <div class="modal-footer">
        <a href="/dosen/kuis/delete/{{$n['id']}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        </div>
      </div> 
    </div>
  </div>
</div>
    @endforeach

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalAdd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Kuis</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('kuis.add')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
        <label for="namakuis">Nama Kuis</label>
        <input type="text" name="name" class="form-control" id="namakuis" placeholder="Masukan Nama Kuis" required="required">
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

</table>
</div>
</div>
</div>
</div>
@endsection