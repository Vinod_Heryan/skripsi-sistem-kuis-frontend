@extends('layouts.dosen')
@section('content')
<div class="d-flex justify-content-center">
<div class="card" style="width: 70%; margin: 2% 0% 2% 0%">
<center>
<img src="{{asset('images/kosong.jpg')}}" width=400 height=400>
</center>

<button type="button" class="btn btn-info" style="margin: 2% 0% 0% 0% " data-toggle="modal" data-target="#myModalAdd">Tambah Kuis</i></button>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalAdd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Kuis</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('kuis.add')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
        <label for="namakuis">Nama Kuis</label>
        <input type="text" name="name" class="form-control" id="namakuis" placeholder="Masukan Nama Kuis" required="required">
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>
</div>
</div>
</div>
@endsection