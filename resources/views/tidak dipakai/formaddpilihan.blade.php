@extends('layouts.dosen')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Tambah Pilihan</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('pilihan.add')}}" method="post">
{{csrf_field()}}
    <div class="form-group">
        <input type="hidden" name="soal_id" class="form-control" value="{{$data['id']}}" required="required">
    
        <label for="jawabansoal">Jawaban :</label>
        <input type="text" name="jawaban" class="form-control" id="jawabansoal" placeholder="Masukan Jawaban" required="required">
    </br>
        <label for="jawabansoal">Kondisi :</label>
        <select class="form-select" aria-label="Default select example" name="kondisi">
        <option selected>Pilih Kondisi</option>
        <option value="1">Benar</option>
        <option value="0">Salah</option>
        </select>
  
    </div>
        <input type="submit" class="btn btn-primary" value="Submit">
</form> 
</table>
</div>
@endsection