@extends('layouts.dosen')
@section('content')
<div class="d-flex justify-content-center">
<div class="card" style="width: 95%; margin-top: 3%;">
<div class="card-body">
<div class="table-responsive">
<table class="table table-bordered" width="100%" >
<thead>
    <tr>
        <th class="text-center" width="5%">NO</td>
        <th class="text-center" width="20%">NIM</td>
        <th class="text-center" width="30%">NAMA MAHASISWA</th>
        <th class="text-center" width="30%">NAMA SESI KUIS</th>
        <th class="text-center" width="15%">SKOR</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td class="text-center">{{$d['NIM']}}</td>
        <td class="text-center">{{$d['nama_mahasiswa']}}</td>
        <td class="text-center">{{$d['nama_sesi']}}</td>
        <td class="text-center">{{$d['skor']}}</td>
    </tr>
    @endforeach
</table>
</div>
</div>
</div>
</div>
@endsection