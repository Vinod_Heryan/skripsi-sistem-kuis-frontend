@extends('layouts.dosen')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
             @foreach ($errors->all() as $error)
             <button type="button" class="close" data-dismiss="alert">×</button>	
              <li>{{ $error }}</li>
             @endforeach
    </div>
@endif

<div class="card-header">
    <h2>Update Kuis</h2>
</div>
<div class="card-body">
<table>
<form action="{{route('kuis.update')}}" method="post">
{{csrf_field()}}
        <input type="hidden" name="id" class="form-control" value="{{$id}}" required="required">

    <div class="form-group">
        <label for="namakuis">Nama Kuis</label>
        <input type="text" name="name" class="form-control" value="{{$nama}}" id="namakuis" placeholder="Masukan Nama Admin" required="required">
    </div>
        <input type="submit" class="btn btn-primary" value="Simpan Data">
</form> 
</table>
</div>
@endsection