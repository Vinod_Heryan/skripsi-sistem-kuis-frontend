@extends('layouts.dosen')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<nav class="navbar navbar-light bg-light">
</nav>
<?php
    $no=1;
?>
<div style="font-family: 'Poppins" >
<a data-toggle="modal" data-target="#myModalAddSoal" class="btn btn-info text-white" style="margin: 2% 0% 2% 5% " title="Tambah Soal"><i class="fa fa-plus-square"></i></a>
	<?php
    for($x = 0; $x < count($data); $x++){
		$soal[$x] = $data[$x]['0']['soal'];
		$id_soal[$x] = $data[$x]['0']['id_soal'];
		$jum[$x] = count($data[$x]['1']);
	?>
       <div class="d-flex justify-content-center">
	   <div class="card" style="width: 90%;">
	   <div class="row-1">
	   <span><a data-toggle="modal" data-target="#myModalAddPilihan{{$id_soal[$x]}}" class="btn btn-info text-white" title="Tambah Pilihan"><i class="fa fa-plus-square"></i></a></span>
	   <span><a data-toggle="modal" data-target="#myModalUpdateSoal{{$id_soal[$x]}}" class="btn btn-info text-white" title="Edit Soal"><i class="fas fa-edit"></i></a></span>
     <span><a data-toggle="modal" data-target="#myModalHapusSoal{{$id_soal[$x]}}" class="btn btn-info text-white" title="Hapus Soal"><i class="fas fa-trash"></i></a></span>
	   </div>
	   <div class="card-body">
		<div class="d-flex justify-content-center">
    <h4><strong>{{$no++}}.</strong></h4>&nbsp;&nbsp;<p class="blockquote" style="width: 85%;">{{$soal[$x]}}</p>
		</div>
		
		<div class="row">
		<?php
    		for($j = 0; $j < $jum[$x]; $j++){
			$jawaban[$j] = $data[$x]['1'][$j]['jawaban'];
			$id_jawaban[$j] = $data[$x]['1'][$j]['id_jawaban'];
			$kondisi[$j] = $data[$x]['1'][$j]['kondisi'];
	
		?>	
			@if($kondisi[$j]==1)
			<span class="card text-white bg-info mb-3" style="width: 20%; padding: 0% 2% 0% 2%; margin: 2% 15% 2% 15%;">

			<div class="d-flex justify-content-center">
        <a data-toggle="modal" data-target="#myModalUpdatePilihan{{$id_jawaban[$j]}}" class="btn btn-info" title="Edit Pilihan"><i class="fas fa-edit"></i></a>
        <a data-toggle="modal" data-target="#myModalHapusPilihan{{$id_jawaban[$j]}}" class="btn btn-info" title="Hapus Pilihan"><i class="fas fa-trash"></i></a>
      </div>

    		<h6 class="text-center">{{$jawaban[$j]}}</h6>

<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdatePilihan{{$id_jawaban[$j]}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Pilihan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('pilihan.update')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="id" class="form-control" value="{{$id_jawaban[$j]}}" required="required">
    
        <label for="jawabansoal">Jawaban :</label>
        <input type="text" name="jawaban" class="form-control" id="jawabansoal" placeholder="Masukan Jawaban" value="{{$jawaban[$j]}}" required="required">
    	</br>
        <label for="jawabansoal">Kondisi :</label>
        <select class="form-select" aria-label="Default select example" name="kondisi">
        <option value="1">Benar</option>
        <option value="0">Salah</option>
        </select>
    </div>
    </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalHapusPilihan{{$id_jawaban[$j]}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Pilihan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="text-center">" {{$jawaban[$j]}} "</div>
        </div>
        <div class="modal-footer">
        <a href="/dosen/kuis/soal/pilihan/delete/{{$id_jawaban[$j]}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        </div>
      </div> 
    </div>
  </div>
</div>
			</span>
			@else
			<span class="card text-white bg-secondary mb-3" style="width: 20%; padding: 0% 2% 0% 2%; margin: 2% 15% 2% 15%;">

			<div class="d-flex justify-content-center">
        <a data-toggle="modal" data-target="#myModalUpdatePilihan{{$id_jawaban[$j]}}" class="btn btn-secondary" title="Edit Pilihan"><i class="fas fa-edit"></i></a>
        <a data-toggle="modal" data-target="#myModalHapusPilihan{{$id_jawaban[$j]}}" class="btn btn-secondary"  title="Hapus Pilihan"><i class="fas fa-trash"></i></a>
      </div>

    		<h6 class="text-center">{{$jawaban[$j]}}</h6>

<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdatePilihan{{$id_jawaban[$j]}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Pilihan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('pilihan.update')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="id" class="form-control" value="{{$id_jawaban[$j]}}" required="required">
    
        <label for="jawabansoal">Jawaban :</label>
        <input type="text" name="jawaban" class="form-control" id="jawabansoal" placeholder="Masukan Jawaban" value="{{$jawaban[$j]}}" required="required">
    	</br>
		<label for="jawabansoal">Kondisi :</label>
        <select class="form-select" aria-label="Default select example" name="kondisi">
        <option value="0">Salah</option>
        <option value="1">Benar</option>
        </select> 
    </div>
    </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalHapusPilihan{{$id_jawaban[$j]}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Pilihan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="text-center">" {{$jawaban[$j]}} "</div>
        </div>
        <div class="modal-footer">
        <a href="/dosen/kuis/soal/pilihan/delete/{{$id_jawaban[$j]}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        </div>
      </div> 
    </div>
  </div>
</div>
			</span>
			@endif
		<?php
		}
		?>
		</div>
		</div>
		</div>	
		</div>

<div class="container ">
  <!-- Modal -->
  <div class="modal fade" id="myModalAddPilihan{{$id_soal[$x]}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Pilihan</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('pilihan.add')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="soal_id" class="form-control" value="{{$id_soal[$x]}}" required="required">
    
        <label for="jawabansoal">Jawaban :</label>
        <input type="text" name="jawaban" class="form-control" id="jawabansoal" placeholder="Masukan Jawaban" required="required">
    	</br>
        <label for="jawabansoal">Kondisi :</label>
        <select class="form-select" aria-label="Default select example" name="kondisi">
        <option selected value="">Pilih Kondisi</option>
        <option value="1">Benar</option>
        <option value="0">Salah</option>
        </select>
	</div>
    </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>



<div class="container ">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdateSoal{{$id_soal[$x]}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Soal</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('soal.update')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="id" class="form-control" value="{{$id_soal[$x]}}" required="required">
        <label for="soalkuis">Soal :</label>
		<textarea name="soal" class="form-control" placeholder="Masukan Soal Disini" id="soalkuis"required="required">{{$soal[$x]}}</textarea>
    	</div>
    	</div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>


<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalHapusSoal{{$id_soal[$x]}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Soal</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="text-center">" {{$soal[$x]}} "</div>
        </div>
        <div class="modal-footer">
        <a href="/dosen/kuis/soal/delete/{{$id_soal[$x]}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        </div>
      </div> 
    </div>
  </div>
</div>

	<?php
	}
	?>	     
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalAddSoal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Soal</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('soal.add')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="kuis_id" class="form-control" value="{{$id['id_kuis']}}" required="required">
        <label for="soalkuis">Soal :</label>
        <textarea name="soal" class="form-control" placeholder="Masukan Soal Disini" id="soalkuis"required="required"></textarea>
    	</div>
		</div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>

@endsection