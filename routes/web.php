<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/mahasiswa', ['middlware' => 'mahasiswa', function () {
    return view('mahasiswa/home');
}]);

Route::get('/dosen', ['middlware' => 'dosen', function () {
    return view('layouts/dosen');
}]);

Route::get('/', [App\Http\Controllers\AuthController\authController::class, 'login_page'])->name('halamanlogin');

Route::post('/login', [App\Http\Controllers\AuthController\authController::class, 'login'])->name('login');
Route::get('/login/tes', [App\Http\Controllers\AuthController\authController::class, 'tes'])->name('tes');
Route::get('/logout', [App\Http\Controllers\AuthController\authController::class, 'logout'])->name('logout');

Route::get('/tesadmin', [App\Http\Controllers\tesController::class, 'tesadmin']);
Route::get('/tes', [App\Http\Controllers\tesController::class, 'tes']);


//---------------------------dosen-------------------------------------------

//---------------------------daftar kuis-------------------------------------

Route::get('/dosen/kuis', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanKuis'])->name('kuis'); 

// Route::get('/dosen/kuis/pagenext', [App\Http\Controllers\DosenController\dosenKuisController::class, 'pageNextKuis'])->name('kuis.next');

// Route::get('/dosen/kuis/pageback', [App\Http\Controllers\DosenController\dosenKuisController::class, 'pageBackKuis'])->name('kuis.back');

// Route::get('/dosen/kuis/update/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanUpdateKuis']);

// Route::get('/dosen/kuis/soal/update/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanUpdateSoal']);

// Route::get('/dosen/kuis/soal/pilihan/update/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanUpdatePilihan']);

// Route::get('/dosen/kuis/add', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanAddKuis'])->name('kuis.hal.add');

Route::get('/dosen/kuis/soal/detail/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'detailKuis'])->name('kuis.detail'); ;

// Route::get('/dosen/kuis/soal/add/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanAddSoal']);

// Route::get('/dosen/kuis/soal/pilihan/add/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'halamanAddPilihan']);

Route::post('/dosen/kuis/add', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesAddKuis'])->name('kuis.add');

Route::post('/dosen/kuis/soal/add', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesAddSoal'])->name('soal.add');

Route::post('/dosen/kuis/soal/pilihan/add', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesAddPilihan'])->name('pilihan.add');

Route::post('/dosen/kuis/update', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesUpdateKuis'])->name('kuis.update');

Route::post('/dosen/kuis/soal/update', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesUpdateSoal'])->name('soal.update');

Route::post('/dosen/kuis/soal/pilihan/update', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesUpdatePilihan'])->name('pilihan.update');

Route::get('/dosen/kuis/search', [App\Http\Controllers\DosenController\dosenKuisController::class, 'searchKuis'])->name('kuis.search');

Route::get('/dosen/kuis/soal/pilihan/delete/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesDeletePilihan']);

Route::get('/dosen/kuis/soal/delete/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesDeleteSoal']);

Route::get('/dosen/kuis/delete/{id}', [App\Http\Controllers\DosenController\dosenKuisController::class, 'prosesDeleteKuis']);

//---------------------------Membuat Tes Kuis-------------------------------------

Route::get('/dosen/sesi/add', [App\Http\Controllers\DosenController\dosenSesiController::class, 'halamanAddTesKuis'])->name('sesi.hal.add');

Route::post('/dosen/sesi/add', [App\Http\Controllers\DosenController\dosenSesiController::class, 'prosesAddTesKuis'])->name('sesi.add');

//---------------------------Grup Sesi---------------------------------------------

Route::get('/dosen/grup', [App\Http\Controllers\DosenController\dosenSesiController::class, 'halamanGrupSesi'])->name('grup');

Route::get('/dosen/grup/search', [App\Http\Controllers\DosenController\dosenSesiController::class, 'searchGrupSesi'])->name('grup.search');

Route::get('/dosen/grup/detail/{grup}', [App\Http\Controllers\DosenController\dosenSesiController::class, 'halamanDetailGrupSesi'])->name('grup.detail');

// Route::get('/dosen/grup/detail/sesi/update/{id}', [App\Http\Controllers\DosenController\dosenSesiController::class, 'halamanUpdateSesi']);

Route::post('/dosen/grup/detail/update/status', [App\Http\Controllers\DosenController\dosenSesiController::class, 'updateStatusSesi'])->name('update.status.sesi');

Route::post('/dosen/grup/detail/sesi/update', [App\Http\Controllers\DosenController\dosenSesiController::class, 'prosesUpdateSesi'])->name('sesi.update');

Route::post('/dosen/grup/sesi/name/update', [App\Http\Controllers\DosenController\dosenSesiController::class, 'prosesUpdateGrup'])->name('grup.update');

Route::get('/dosen/grup/sesi/delete/{id}', [App\Http\Controllers\DosenController\dosenSesiController::class, 'prosesDeleteSesi']);

//---------------------------Nilai------------------------------------------------------

Route::get('/dosen/nilai/{id}', [App\Http\Controllers\DosenController\dosenNilaiController::class, 'getDataHasilKuis'])->name('nilai');;

Route::get('/dosen/nilai/rekap/{id}', [App\Http\Controllers\DosenController\dosenNilaiController::class, 'rekapNilai']);

Route::get('/dosen/nilai/rekap/excel/{id}', [App\Http\Controllers\DosenController\dosenNilaiController::class, 'rekapNilaiToExcel']);

Route::get('/dosen/nilai/rekap/semua/{grup}', [App\Http\Controllers\DosenController\dosenNilaiController::class, 'rekapSemuaNilai']);

Route::get('/dosen/nilai/rekap/semua/excel/{grup}', [App\Http\Controllers\DosenController\dosenNilaiController::class, 'rekapSemuaNilaiToExcel']);

//---------------------------Mahasiswa-------------------------------------------

Route::get('/mahasiswa/kuis', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'kuisMhs'])->name('kuis.mhs');

Route::get('/mahasiswa/kuis/tes/soal', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'tesKuisMhs'])->name('kuis.mhs.tes');

Route::get('/mahasiswa/nilai', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'lihatHasilKuis'])->name('nilai.mhs');

Route::get('/mahasiswa/nilai/{grup}', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'lihatHasilKuisByGrup'])->name('nilai.mhs.grup');

// Route::get('/mahasiswa/cek/nilai', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'lihatHasilKuis'])->name('nilai.mhs');

Route::post('/mahasiswa/kuis/tes/soal/jawab', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'tesKuisMhsPage'])->name('jawab');

Route::post('/mahasiswa/kuis/tes/soal/selesai', [App\Http\Controllers\MahasiswaController\mahasiswaController::class, 'kuisSelesai'])->name('selesai');