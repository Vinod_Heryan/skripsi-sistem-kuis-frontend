<?php

namespace App\Http\Controllers\DosenController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class dosenSesiController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('dosen');
    }

    public function halamanAddTesKuis()
    {
        $id = session('id_dosen');
        $url_kuis ="http://localhost:8000/dosen/kuis/$id";
        $url_grup ="http://localhost:8000/dosen/sesi/grup/$id";
        $token = session('token');
        $kuis = Http::withToken($token)->get($url_kuis)['data'];
        $grup = Http::withToken($token)->get($url_grup)['data'];

        return view('dosen/formaddteskuis',compact('kuis','grup'));

        // return dd($kuis);
    }

    public function prosesAddTesKuis(Request $request)
    {
        $token = session('token');

        if($request->kuis_id == null)
            {

                return back()->with('error','Tambah Data Gagal Inputan Pilih Kuis Tidak Boleh Kosong !!!');

            }

        if($request->grupbaru == null && $request->grup == null)
            {

                return back()->with('error','Tambah Data Gagal Isi Salah Satu Dari Inputan Grup Dan GrupBaru !!!');

            }

        if($request->grup == null || $request->grupbaru == null)
        {
            if($request->grup == null)
            {
                $data = Http::withToken($token)->post('http://localhost:8000/dosen/sesi/add', [
                    'name' => $request->name,
                    'kuis_id' => $request->kuis_id,
                    'grup' => $request->grupbaru,
                ]);

                return back()->with('success','Data Berhasil Ditambah !!!');

            }else if($request->grupbaru == null)
            {
                $data = Http::withToken($token)->post('http://localhost:8000/dosen/sesi/add', [
                    'name' => $request->name,
                    'kuis_id' => $request->kuis_id,
                    'grup' => $request->grup,
                ]);

                return back()->with('success','Data Berhasil Ditambah !!!');

            }

        }
       
        return back()->with('error','Tambah Data Gagal, Isi Salah Satu Saja Dari Inputan Grup Dan GrupBaru !!!');
    }

    public function halamanGrupSesi()
    {
        $id = session('id_dosen');
        $url_grup ="http://localhost:8000/dosen/sesi/grup/$id";
        $token = session('token');
        $grup = Http::withToken($token)->get($url_grup)['data'];

        if($grup== null)
        {
            return view('dosen/kosong/datakosong');
        }

        return view('dosen/datagrupsesi',compact('grup'));

        // return dd($grup);
    }

    public function searchGrupSesi(Request $request)
    {
        $id = session('id_dosen');
        $url_grup ="http://localhost:8000/dosen/sesi/grup/$id/$request->search";
        $token = session('token');
        $grup = Http::withToken($token)->get($url_grup)['data'];

        return view('dosen/datagrupsesi',compact('grup'));

        // return dd($kuis);
    }

    public function halamanDetailGrupSesi($grup)
    {
        $id = session('id_dosen');
        $url ="http://localhost:8000/dosen/sesi/grup/detail/$id/$grup";
        $url_kuis ="http://localhost:8000/dosen/kuis/$id";
        $token = session('token');
        $data = Http::withToken($token)->get($url)['data'];
        $kuis = Http::withToken($token)->get($url_kuis)['data'];

        return view('dosen/datadetailgrupsesi',compact('data','kuis'));

        // return dd($kuis);
    }

    public function updateStatusSesi(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/sesi/grup/detail/update/status', [
            'id'=>$request->id,
            'status'=>$request->status,
        ]);

        return redirect()->back()->with('success','Data Berhasil Diupdate !!!');
    }

    public function prosesUpdateSesi(Request $request)
    {
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/sesi/grup/detail/sesi/update', [
            'id'=>$request->id,
            'name'=> $request->name,
            'kuis_id'=> $request->kuis_id,
        ]);

        return redirect()->back()->with('success','Data Berhasil Diupdate !!!');
    }

    public function prosesUpdateGrup(Request $request)
    {
        $id_dosen = session('id_dosen');
        $token = session('token');
        $data = Http::withToken($token)->put('http://localhost:8000/dosen/sesi/grup/update/name', [
            'dosen_id'=>$id_dosen,
            'grup'=> $request->grup,
            'grupbaru'=> $request->grupbaru,
        ]);

        return redirect()->back()->with('success','Data Berhasil Diupdate !!!');
    }

    public function prosesDeleteSesi($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/sesi/nilai/delete/$id";
        $data = Http::withToken($token)->delete($url);

        return back()->with('success','Data Berhasil DiHapus !!!');
    }

    // public function halamanUpdateSesi($id)
    // {
    //     $id_dosen = session('id_dosen');
    //     $token = session('token');
    //     $url_kuis ="http://localhost:8000/dosen/kuis/$id_dosen";
    //     $url ="http://localhost:8000/dosen/sesi/grup/detail/sesi/byid/$id";
    //     $data = Http::withToken($token)->get($url)['data'];
    //     $kuis = Http::withToken($token)->get($url_kuis)['data'];

    //     // return dd($data);

    //     return view('dosen/formupdatesesi',compact('data','kuis'));
    // }
}
