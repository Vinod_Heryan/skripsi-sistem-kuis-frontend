@extends('layouts.dosen')
@section('content')
@if($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
@if($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
<div style="margin: 0% 2% 0% 2%; padding: 1% 0% 1% 0% ">
    <h2>Update Sesi Tes Kuis</h2>
</div>
<div class="d-flex justify-content-center">
<div class="card" style="width: 90%;">
<div class="card-body">
<table>
<form action="{{route('sesi.update')}}" method="post">
{{csrf_field()}}
    <div class="form-group">

    <input type="hidden" name="id" class="form-control" value="{{$data['id']}}" required="required">

        <label for="namakuis">Nama Sesi :</label>
        <input type="text" name="name" class="form-control" value="{{$data['nama_sesi']}}" id="namakuis" placeholder="Masukan Nama Admin" required="required">
    </br>
        <label for="namakuis">Pilih Kuis :</label>
        <select class="form-control" aria-label="Default select example" name="kuis_id">
            <option value="{{$data['kuis_id']}}" selected>{{$data['nama_kuis']}}</option>
        <?php
    		for($x = 0; $x < count($kuis); $x++){
			    $kuis_id[$x] = $kuis[$x]['id'];
                $nama_kuis[$x] = $kuis[$x]['name'];
		?>
			<option value="{{$kuis_id[$x]}}">{{$nama_kuis[$x]}}</option>
		<?php
		}
		?> 
        </select>
    </br>
        <input type="submit" class="btn btn-primary" value="Submit">
</form> 
</table>
</div>
</div>
</div>
@endsection