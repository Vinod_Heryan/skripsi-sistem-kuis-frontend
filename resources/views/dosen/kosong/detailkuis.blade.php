@extends('layouts.dosen')
@section('content')
<div class="d-flex justify-content-center">
<div class="card" style="width: 70%; margin: 2% 0% 2% 0%">
<center>
<img src="{{asset('images/kosong.jpg')}}" width=400 height=400>
</center>

<button type="button" class="btn btn-info" style="margin: 2% 0% 0% 0% " data-toggle="modal" data-target="#myModalAdd">Tambah Soal</i></button>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalAdd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Soal</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('soal.add')}}" method="post">
		{{csrf_field()}}
    	<div class="form-group">
        <input type="hidden" name="kuis_id" class="form-control" value="{{$id['id_kuis']}}" required="required">
        <label for="soalkuis">Soal :</label>
        <textarea name="soal" class="form-control" placeholder="Masukan Soal Disini" id="soalkuis"required="required"></textarea>
    	</div>
		</div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>
</div>
</div>
@endsection