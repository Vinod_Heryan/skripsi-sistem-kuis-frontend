<div><h1>Nilai Hasil Kuis Mahasiswa</h1></div>

<table border="1" width="100%" >
<thead>
    <tr>
        <th  width="5%">NO</th>
        <th  width="20%">NIM</th>
        <th  width="30%">NAMA MAHASISWA</th>
        <th  width="30%">NAMA SESI KUIS</th>
        <th  width="15%">SKOR</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td class="text-center">{{$d['NIM']}}</td>
        <td class="text-center">{{$d['nama_mahasiswa']}}</td>
        <td class="text-center">{{$d['nama_sesi']}}</td>
        <td class="text-center">{{$d['skor']}}</td>
    </tr>
    @endforeach
</table>

<style>
td {
    text-align: center;
}
h1 {
    text-align: center;
    text-decoration: underline;
}
</style>