@extends('layouts.mahasiswa')
@section('content')
<div class="d-flex justify-content-center">
<div class="card" style="width: 95%; margin-top: 3%;">
<div class="card-body">
<nav class="navbar d-flex justify-content-end">

<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Pilih Grup Sesi
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    @foreach($grup as $g)
        <a class="dropdown-item" href="/mahasiswa/nilai/{{$g['nama_grup']}}">{{$g['nama_grup']}}</a>
    @endforeach
  </div>
</div>

</nav>
<div class="table-responsive">
<table class="table table-bordered" width="100%" >
<thead>
    <tr>
        <th class="text-center" width="10%">NO</td>
        <th class="text-center" width="20%">NIM</td>
        <th class="text-center" width="30%">NAMA GRUP SESI</th>
        <th class="text-center" width="30%">NAMA SESI KUIS</th>
        <th class="text-center" width="10%">SKOR</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td class="text-center">{{$d['NIM']}}</td>
        <td class="text-center">{{$d['nama_grup']}}</td>
        <td class="text-center">{{$d['nama_sesi']}}</td>
        <td class="text-center">{{$d['skor']}}</td>
    </tr>
    @endforeach
</table>
</div>
</div>
</div>
</div>
@endsection