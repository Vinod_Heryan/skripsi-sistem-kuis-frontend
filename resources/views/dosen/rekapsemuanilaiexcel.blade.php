<table>
<thead>
    <tr>
        <th>NO</th>
        <th>NIM</th>
        @foreach($sesi as $t)
        <th>{{$t['nama_sesi']}}</th>
        @endforeach
        <th>RATA-RATA</th>    
    </tr>
</thead>
        
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td class="text-center">{{$no++}}</td>
        <td class="text-center">{{$d['datamhs']['NIM']}}</td>
        <?php
    		for($x = 0; $x < count($d['skor']); $x++){
                $skor[$x] = $d['skor'][$x];
		?>
			<td class="text-center">{{$skor[$x]}}</td>
		<?php
		}
		?> 
        <td class="text-center">{{$d['skorakhir']}}</td>
    </tr>
    @endforeach
</table>
