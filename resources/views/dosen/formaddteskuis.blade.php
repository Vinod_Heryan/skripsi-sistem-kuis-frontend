@extends('layouts.dosen')
@section('content')
@if($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
@if($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
	    <button type="button" class="close" data-dismiss="alert">x</button>	
		<strong>{{ $message }}</strong>
	  </div>
@endif
<div style="margin: 0% 2% 0% 2%; padding: 1% 0% 1% 0% ">
    <h3>Membuat Sesi Tes Kuis</h3>
</div>
<div class="d-flex justify-content-center">
<div class="card" style="width: 90%;">
<div class="card-body">
<table>
<form action="{{route('sesi.add')}}" method="post">
{{csrf_field()}}
    <div class="form-group">
        <label for="namakuis">Nama Sesi :</label>
        <input type="text" name="name" class="form-control" id="namakuis" placeholder="Masukan Nama Sesi" required="required">
    </br>
        <label for="namakuis">Pilih Kuis :</label>
        <select class="form-control" aria-label="Default select example" name="kuis_id">
            <option value=" " selected>-- Pilih Kuis --</option>
        <?php
    		for($x = 0; $x < count($kuis); $x++){
			    $kuis_id[$x] = $kuis[$x]['id'];
                $nama_kuis[$x] = $kuis[$x]['name'];
		?>
			<option value="{{$kuis_id[$x]}}">{{$nama_kuis[$x]}}</option>
		<?php
		}
		?> 
        </select>
    </br>
        <label for="namakuis">Pilih Grup :</label>
        <select class="form-control" aria-label="Default select example" name="grup">
            <option value=" " selected>-- Pilih Grup Sesi --</option>
            <?php
    		for($j = 0; $j < count($grup); $j++){
                $nama_grup[$j] = $grup[$j]['grup'];
		?>
			<option value="{{$nama_grup[$j]}}">{{$nama_grup[$j]}}</option>
		<?php
		}
		?> 
        </select>
    </br>
        <label for="newgroup">Tambah Grup Baru :</label>
        <input type="text" name="grupbaru" class="form-control" id="newgroup" placeholder="Masukan Grup Baru">
        <small class="form-text text-muted">Jika Grup Yang Dicari Tidak Tersedia Pada Pilih Grup, Maka Grup Bisa Ditambahkan Disini</small>
    </div>
    <div class="d-flex justify-content-end">
        <input type="submit" class="btn btn-primary" value="Submit">
    </div>    
</form> 
</table>
</div>
</div>
</div>
@endsection