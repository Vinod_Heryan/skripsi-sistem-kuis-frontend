<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('skripsi/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('skripsi/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
<title>Login</title>
</head>
<!------ Include the above in your HEAD tag ---------->
@if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<body>
    <div id="login">
        <div class="d-flex justify-content-center">
        <img src="{{asset('images/ukrim.png')}}" style="margin-top: 5%;" width="150px" height="150px" alt="AdminLTE Logo">
        </div>
        <h2 class="text-center text-dark pt-4">Sistem Kuis Mahasiswa</h2>
        <div class="container" style="width:60%">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{{ route('login') }}" method="post">
                        @csrf
                            <div class="form-group">
                                <label for="no_induk" class="text-dark">Nomor Induk :</label><br>
                                <input id="no_induk" type="text" name="no_induk" value="{{ old('no_induk') }}" required autocomplete="no_induk" autofocus class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-dark">Password :</label><br>
                                <input id="password" type="password" name="password" required autocomplete="current-password"class="form-control">
                            </div>
                            <div class="form-group">
                              <center>
                                <input type="submit" name="submit" class="btn btn-dark btn-md" value="Masuk">
                              </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<style>
body {
  margin: 0;
  padding: 0;
  background-color: #EAEAEA;
  height: 100vh;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 10px;
}
</style>