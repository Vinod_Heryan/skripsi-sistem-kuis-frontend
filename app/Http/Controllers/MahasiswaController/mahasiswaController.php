<?php

namespace App\Http\Controllers\mahasiswaController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Session;

class mahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('mahasiswa');
    }

    // public function formceknilai()
    // {
    //     return view('mahasiswa/formcekhasilkuis');
    // }

    public function kuisMhs()
    {
        return view('mahasiswa/home');
    }

    public function tesKuisMhs(Request $request)
    {
        session(['kode' => $request->kode]);
        session(['nilai' => 0]);
        session(['page' => 1]);

        $token = session('token');
        $no_induk = session('no_induk');
        $url_cek_mahasiswa ="http://localhost:8000/mahasiswa/nilai/cek/$request->kode/$no_induk";
        $cek = Http::withToken($token)->get($url_cek_mahasiswa);
        $url ="http://localhost:8000/mahasiswa/tes/soal/kuis/$request->kode";
        $data = Http::withToken($token)->get($url);

        if($data['data'] == null)
        {
            return back()->with('error','Kode Kuis Tidak Valid..!!!');

        }

        if($data['data']['data']['0']['status'] == 0)
        {
            return back()->with('error','Kuis Tidak Aktif..!!!');

        }

        if($cek['data'] != null && $data['data']['data']['0']['status'] == 1)
        {
            return back()->with('error','Anda Hanya Bisa Mengikuti Sesi Kuis Ini Satu Kali Saja..!!!');

        } 

        session(['id_sesi' => $data['data']['data']['0']['id_sesi']]);

        $halaman = $data['data']['current_page'];
        $halaman_terakhir = $data['data']['last_page'];

        session(['jumlah_soal' => $halaman_terakhir]);



        if($data['data']['data']['0']['status'] == 0)
        {
            Session::flash('error','Kuis Sudah Tidak Aktif Lagi Silahkan Klik Tombol Selesai..!!!');

            return view('mahasiswa/teskuis',compact('data','halaman','halaman_terakhir'));

        }
        
        return view('mahasiswa/teskuis',compact('data','halaman','halaman_terakhir'));

        // return dd($data['data']);

        // return $gambar;
    }

    public function tesKuisMhsPage(Request $request)
    {

        if($request->nomor != session('page'))
        { 
            Session::flash('error','Tidak Boleh Melakukan Back Page...!!!');
            $page = session('page');
            $kode = session('kode');
            $url ="http://localhost:8000/mahasiswa/tes/soal/kuis/$kode?page=$page";
            $token = session('token');
            $data = Http::withToken($token)->get($url);
            $halaman = $data['data']['current_page'];
            $halaman_terakhir = $data['data']['last_page'];

            return view('mahasiswa/teskuis',compact('data','halaman','halaman_terakhir'));
        }

        $cekjawaban ="http://localhost:8000/mahasiswa/tes/soal/kuis/kondisi/pilihan/$request->jawaban";
        $token = session('token');
        $kondisi = Http::withToken($token)->get($cekjawaban)['data'];
            
        if($kondisi['kondisi'] == 1)
        {
            session(['nilai' => session('nilai') + 1]);
        }
        
        session(['page' => session('page') + 1]);

        $page = session('page');
        $kode = session('kode');

        $url ="http://localhost:8000/mahasiswa/tes/soal/kuis/$kode?page=$page";
        $token = session('token');
        $data = Http::withToken($token)->get($url);

        // if($data['data']==null)
        // {
        //     return redirect()->route('kuis.mhs')->with('error','Tidak Boleh Melakukan Back Page, Mohon Untuk Mengulang Kembali Kuis Dari Awal !!!');
        // }

        $halaman = $data['data']['current_page'];
        $halaman_terakhir = $data['data']['last_page'];

        if($data['data']['data']['0']['status'] == 0)
        {
            Session::flash('error','Kuis Sudah Tidak Aktif Lagi Silahkan Klik Tombol Selesai..!!!');

            return view('mahasiswa/teskuis',compact('data','halaman','halaman_terakhir'));

        }

        return view('mahasiswa/teskuis',compact('data','halaman','halaman_terakhir'));

        // return dd($data['pilihan']);

        // return $gambar;
    }

    public function kuisSelesai(Request $request)
    {
        $cekjawaban ="http://localhost:8000/mahasiswa/tes/soal/kuis/kondisi/pilihan/$request->jawaban";
        $token = session('token');
        $kondisi = Http::withToken($token)->get($cekjawaban)['data'];
            
        if($kondisi['kondisi'] == 1)
        {
            session(['nilai' => session('nilai') + 1]);
        }

        $nilai = Round((session('nilai') * 100) / session('jumlah_soal'));

        $token = session('token');
        $data = Http::withToken($token)->post('http://localhost:8000/mahasiswa/tes/soal/kuis/nilai/add', [
            'sesi_id' => session('id_sesi'),
            'NIM' => session('no_induk'),
            'name' => session('name'),
            'skor' => $nilai,   
        ]);

        return redirect()->route('kuis.mhs')->with('success','Kuis Sudah Selesai Silahkan Cek Nilai Anda.');

    }

    public function lihatHasilKuis()
    {
        $token = session('token');
        $no_induk = session('no_induk');
        $url ="http://localhost:8000/mahasiswa/nilai/$no_induk";
        $url_grup ="http://localhost:8000/mahasiswa/grup/$no_induk";
        $data = Http::withToken($token)->get($url)['data'];
        $grup = Http::withToken($token)->get($url_grup)['data'];

        // return dd($data);

        if($data == null)
        {
            return view('mahasiswa/kosong/datakosong');

        }

        return view('mahasiswa/datanilai',compact('data','grup'));
    }

    public function lihatHasilKuisByGrup($grup)
    {
        $token = session('token');
        $no_induk = session('no_induk');
        $url ="http://localhost:8000/mahasiswa/nilai/$no_induk/$grup";
        $url_grup ="http://localhost:8000/mahasiswa/grup/$no_induk";
        $data = Http::withToken($token)->get($url)['data'];
        $grup = Http::withToken($token)->get($url_grup)['data'];

        return view('mahasiswa/datanilai',compact('data','grup'));
        // return $data;
    }
}
