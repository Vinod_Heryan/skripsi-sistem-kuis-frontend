<?php

namespace App\Http\Controllers\AuthController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class authController extends Controller
{
    public function login_page()
    {
        return view('layouts/login');
    }
    
    public function login(Request $request)
    {
        try{
            $data = Http::post('http://localhost:8000/login', [
                'no_induk' => $request->no_induk,
                'password' => $request->password, 
            ]);

            session(['token' => $data['token']]);
            session(['id_dosen' => $data['id']]);
            session(['no_induk' => $data['no_induk']]);
            session(['name' => $data['name']]);
            session(['is_admin' => $data['is_admin']]);

        }catch(\Exception $e){

            return redirect()->route('halamanlogin')->with('error','Nomor Induk dan Password Tidak Valid !!!');

        } 
            
            if(session('is_admin') == 0){

                $this->middleware('mahasiswa');

                return redirect()->route('kuis.mhs');

            }else{

                $this->middleware('dosen');

                return redirect()->route('kuis');

            }           
    }
 
    public function logout(Request $request)
    {
        $token = session('token');
        Http::withToken($token)->post('http://localhost:8000/logout');
        $request->session()->flush();

        return redirect('/');
    }

    // public function register(Request $request)
    // {
    //      $request->validate([
    //         'name' => 'required',
    //         'email' => 'required|email|unique:users',
    //         'is_admin' => 'required',
    //         'password' => 'required|min:8',
    //     ],[
    //         'email.required' =>'Data Email Tidak Boleh Kosong',
    //         'name.required' =>'Data Nama Tidak Boleh Kosong',
    //         'id_admin.required' =>'Data Is Admin Tidak Boleh Kosong',
    //         'password.required' =>'Data Password Tidak Boleh Kosong',
    //         'email.email' =>'Data Email Harus Berformat Email',
    //         'email.unique:users' =>'Data Email Harus Sudah Digunakan',
    //         'password.min:8' =>'Data Password Minimal 8 Karakter',
    //     ]);

    //     $token = session('token');
    //     return Http::withToken($token)->post('http://localhost:8000/register', [
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'is_admin' => $request->is_admin,
    //         'password' => $request->password,
    //     ]);
    // }

}
