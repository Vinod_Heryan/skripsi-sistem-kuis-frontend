<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Http;

class NilaiExport implements FromView
{
    
    public function view(): View
    {
        $id_dosen = session('id_dosen');
        $grup = session('grup');
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/all/grup/$id_dosen/$grup";
        $data = Http::withToken($token)->get($url)['data'];
        $sesi = Http::withToken($token)->get($url)['sesi'];

        return view('dosen/rekapsemuanilaiexcel', [
            'data' => $data,
            'sesi' => $sesi
        ]);
    }
}
