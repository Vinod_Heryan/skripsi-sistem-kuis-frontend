<?php

namespace App\Http\Controllers\DosenController;

use PDF;
use Excel;
use App\Exports\NilaiExport;
use App\Exports\NilaiSesiExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class dosenNilaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
        $this->middleware('dosen');
    }

    public function getDataHasilKuis($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/$id";
        $data = Http::withToken($token)->get($url)['data'];

        // return dd($data);

        if($data == null)
        {
            return view('dosen/kosong/datakosong');
        }

        return view('dosen/datanilai',compact('data'));
    }

    public function rekapNilai($id)
    {
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/$id";
        $data = Http::withToken($token)->get($url)['data'];

        if($data == null)
        {
            return back()->with('error','Data Nilai Tidak Tersedia...!!!');
        }

        $pdf = PDF::loadview('dosen/rekapnilai',['data'=>$data]);

        return $pdf->download('File-Rekap-Nilai.pdf');
    }

    public function rekapSemuaNilai($grup)
    {
        $id_dosen = session('id_dosen');
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/all/grup/$id_dosen/$grup";
        $data = Http::withToken($token)->get($url)['data'];

        if($data == null)
        {
            return back()->with('error','Data Nilai Tidak Tersedia...!!!');
        }

        $sesi = Http::withToken($token)->get($url)['sesi'];
        
        // return $data;

        $pdf = PDF::loadview('dosen/rekapsemuanilai',['data'=>$data, 'sesi'=>$sesi]);

        // download pdf file
        return $pdf->download('File-Rekap-Semua-Nilai.pdf');

        // return dd($sesi);
    }

    public function rekapNilaiToExcel($id)
    {
        session(['rekap_id' => $id]);
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/$id";
        $data = Http::withToken($token)->get($url)['data'];

        if($data == null)
        {
            return back()->with('error','Data Nilai Tidak Tersedia...!!!');
        }

        return Excel::download(new NilaiSesiExport, 'File-Rekap-Nilai.xlsx');
    }

    public function rekapSemuaNilaiToExcel($grup)
    {
        session(['grup' => $grup]);
        $id_dosen = session('id_dosen');
        $token = session('token');
        $url ="http://localhost:8000/dosen/nilai/all/grup/$id_dosen/$grup";
        $data = Http::withToken($token)->get($url)['data'];

        if($data == null)
        {
            return back()->with('error','Data Nilai Tidak Tersedia...!!!');
        }

        return Excel::download(new NilaiExport, 'File-Rekap-Semua-Nilai.xlsx');
    }

}