@extends('layouts.dosen')
@section('content')
    @if ($message = Session::get('success'))
	  <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		  <strong>{{ $message }}</strong>
	  </div>
	@endif

	@if ($message = Session::get('update'))
	  <div class="alert alert-warning alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif

    @if ($message = Session::get('error'))
	  <div class="alert alert-danger alert-block">
	    <button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	  </div>
	@endif
<div class="d-flex justify-content-center">
<div class="card" style="width: 95%; margin-top: 2%;">
<div class="card-body">
<div class="table-responsive">
<table class="table table-striped table-bordered" width="100%" >
<thead>
    <tr>
        <th width="5%">NO</td>
        <th width="25%">Nama Sesi</th>
        <th width="20%">Nama Kuis</th>
        <th width="15%">Kode Kuis</th>
        <th class="text-center" width="15%">Status</th>
        <th class="text-center" width="20%">Actions</th>
    </tr>
</thead>
    <?php
    $no=1;
    ?>
    @foreach($data as $d)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$d['nama_sesi']}}</td>
        <td>{{$d['nama_kuis']}}</td>
        <td>{{$d['kode']}}</td>
        <td class="text-center">
        @if($d['status']==1)
        <form action="{{route('update.status.sesi')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="id" class="form-control" value="{{$d['id']}}" required="required">
        <input type="hidden" name="status" class="form-control" value="{{$d['status']}}" required="required">
        <input type="submit" class="btn btn-success" value="Aktif" title="Ubah Status Non Aktif">
        </form>        
		    @else
        <form action="{{route('update.status.sesi')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="id" class="form-control" value="{{$d['id']}}" required="required">
        <input type="hidden" name="status" class="form-control" value="{{$d['status']}}" required="required">
        <input type="submit" class="btn btn-secondary" value="Non Aktif" title="Ubah Status Aktif">
        </form>      
		    @endif
        </td>   
        <td class="text-center">
        <button type="button" class="btn btn-outline-success my-2 my-sm-0" data-toggle="modal" data-target="#myModalRekap{{$d['id']}}"  title="Rekap Nilai"><i class="fas fa-file-export"></i></button>
        <button type="button" class="btn btn-outline-warning my-2 my-sm-0" data-toggle="modal" data-target="#myModalUpdate{{$d['id']}}" title="Edit Sesi"><i class="fas fa-edit"></i></button>
        <a href="/dosen/nilai/{{$d['id']}}" class="btn btn-outline-info my-2 my-sm-0" title="Data Nilai"><i class="far fa-file-alt"></i></a>
        <button type="button" class="btn btn-outline-danger my-2 my-sm-0" data-toggle="modal" data-target="#myModalHapus{{$d['id']}}" title="Hapus Sesi"><i class="fas fa-trash"></i></button>
      </td>
    </tr>

    <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalRekap{{$d['id']}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Rekap Nilai</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="d-flex justify-content-center">
            <a href="/dosen/nilai/rekap/{{$d['id']}}" class="btn btn-outline-danger my-2 my-sm-0"  title="Format PDF"><i class="far fa-file-pdf"></i></a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/dosen/nilai/rekap/excel/{{$d['id']}}" class="btn btn-outline-success my-2 my-sm-0"  title="Format Excel"><i class="far fa-file-excel"></i></a>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div> 
    </div>
  </div>
</div>

    <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModalUpdate{{$d['id']}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Update Sesi</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{route('sesi.update')}}" method="post">
        {{csrf_field()}}
      <div class="form-group">

      <input type="hidden" name="id" class="form-control" value="{{$d['id']}}" required="required">

        <label for="namakuis">Nama Sesi :</label>
        <input type="text" name="name" class="form-control" value="{{$d['nama_sesi']}}" id="namakuis" placeholder="Masukan Nama Sesi" required="required">
        </br>
        <label for="namakuis">Pilih Kuis :</label>
        <select class="form-control" aria-label="Default select example" name="kuis_id">
            <option value="{{$d['kuis_id']}}" selected>{{$d['nama_kuis']}}</option>
        <?php
    		for($x = 0; $x < count($kuis); $x++){
			    $kuis_id[$x] = $kuis[$x]['id'];
                $nama_kuis[$x] = $kuis[$x]['name'];
		    ?>
			  <option value="{{$kuis_id[$x]}}">{{$nama_kuis[$x]}}</option>
		    <?php
		    }
		    ?> 
        </select>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        </div>
      </div> 
    </div>
  </div>
</div>


<div class="container text-dark">
  <!-- Modal -->
  <div class="modal fade" id="myModalHapus{{$d['id']}}" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delete Sesi</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="text-center">Sesi " {{$d['nama_sesi']}} " Dan Semua Data Nilainya Akan Dihapus</div>
        </div>
        <div class="modal-footer">
        <a href="/dosen/grup/sesi/delete/{{$d['id']}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
        </div>
      </div> 
    </div>
  </div>
</div>


    @endforeach
</table>
</div>
</div>
</div>
</div>
@endsection